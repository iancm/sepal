/*
 * genetics.h
 *
 * This file manages the classes used to simulate organisms' genomes.
 *
 */
#include <iostream>

enum sex_t
{
	SEXLESS,
	FEMALE,
	MALE,
	HERMAPHRODITE
};

/* A Gene is an atomic unit of genetic information. */
class Gene
{
	public:
		Gene();
		Gene(const Gene &);

		Gene& operator = (const Gene &);
		Gene& operator = (const unsigned char);
		bool operator == (const Gene &)        const;
		bool operator == (const unsigned char) const;
		bool operator != (const Gene &)        const;
		bool operator != (const unsigned char) const;

		friend std::ostream& operator << (std::ostream &, const Gene &);
		friend std::istream& operator >> (std::istream &, Gene &);

		void set_bit(int n);        /* sets bit #n to 1 */
		void unset_bit(int n);      /* sets bit #n to 0 */
		void toggle_bit(int n);     /* toggles bit #n */
		bool is_set(int n) const;   /* returns whether bit #n is set */

		void randomize();
	private:
		unsigned char data;
};

class Chromosome
{
	public:
		Chromosome();
		Chromosome(int length);
		Chromosome(int length, sex_t sex);
		Chromosome(const Chromosome &);
		~Chromosome();

		Chromosome& operator = (const Chromosome &);
		friend std::ostream& operator << (std::ostream &, const Chromosome &);

		void randomize_genes();

		/* Functions targeting Chromosome properties */
		sex_t get_sex()         const;
		void set_sex(sex_t sex);
		bool is_sex(sex_t sex)  const;

		/* Functions targeting individual Genes */
		bool gene_equals(int index, const Gene &)  const;
		bool gene_equals(int index, unsigned char) const;
		void set_bit(int index, int bit);
		void unset_bit(int index, int bit);
		void toggle_bit(int index, int bit);
		bool is_set(int index, int bit) const;

	private:
		Gene * data;
		int len;
		sex_t sex;
};
