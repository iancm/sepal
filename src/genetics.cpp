#include "genetics.h"
#include "pcgrand.h"

using namespace std;

Pcgrand generand = 7;

/* @Gene class */
Gene::Gene() : data(0) {;}
Gene::Gene(const Gene & src) : data(src.data) {;}

Gene& Gene::operator = (const Gene & src)
{
	data = src.data;
	return *this;
}
Gene& Gene::operator = (const unsigned char c)
{
	data = c;
	return *this;
}

bool Gene::operator == (const Gene & x) const
{
	return data == x.data;
}
bool Gene::operator == (const unsigned char c) const
{
	return data == c;
}
bool Gene::operator != (const Gene & x) const
{
	return data != x.data;
}
bool Gene::operator != (const unsigned char c) const
{
	return data != c;
}

ostream& operator << (ostream& out, const Gene & g)
{
	return (out << hex << (int) g.data);
}
istream& operator >> (istream& in, Gene & g)
{
	int i;
	in >> hex >> i;
	g.data = (unsigned char) i;
	return in;
}

void Gene::set_bit(int n)
{
	data |= 1u << n;
}
void Gene::unset_bit(int n)
{
	data &= ~(1u << n);
}
void Gene::toggle_bit(int n)
{
	data ^= 1u << n;
}
bool Gene::is_set(int n) const
{
	return (data & (1u << n));
}

void Gene::randomize()
{
	data = generand.rand();
}

/* @Chromosome class */
Chromosome::Chromosome() : data(NULL), len(0) {;}
Chromosome::Chromosome(int n) : len(n)
{
	data = new Gene[n];
}
Chromosome::Chromosome(int n, sex_t s) : len(n), sex(s)
{
	data = new Gene[n];
}
Chromosome::Chromosome(const Chromosome & src) : len(src.len), sex(src.sex)
{
	data = new Gene[len];
	for(int i = 0; i < len; ++i)
		data[i] = src.data[i];
}
Chromosome::~Chromosome()
{
	delete [] data;
}

Chromosome& Chromosome::operator = (const Chromosome & src)
{
	len = src.len;
	sex = src.sex;
	if(data)
		delete [] data;
	data = new Gene[len];
	for(int i = 0; i < len; ++i)
		data[i] = src.data[i];
	return *this;
}

ostream& operator << (ostream & out, const Chromosome & c)
{
	cout << "len = " << c.len << " sex = ";
	switch(c.sex)
	{
		case SEXLESS:
			cout << "none\n";
			break;
		case MALE:
			cout << "male\n";
			break;
		case FEMALE:
			cout << "female\n";
			break;
		case HERMAPHRODITE:
			cout << "hermaphroditic\n";
			break;
		default:
			cout << "other\n";
	}
	for(int i = 0; i < c.len; ++i)
		cout << c.data[i];
	cout << endl;
	return out;
}

void Chromosome::randomize_genes()
{
	for(int i = 0; i < len; ++i)
		data[i].randomize();
}

sex_t Chromosome::get_sex() const
{
	return sex;
}
void Chromosome::set_sex(sex_t s)
{
	sex = s;
}
bool Chromosome::is_sex(sex_t s) const
{
	return sex == s;
}

bool Chromosome::gene_equals(int index, const Gene & target) const
{
	return (data[index] == target);
}
bool Chromosome::gene_equals(int index, unsigned char target) const
{
	return (data[index] == target);
}
void Chromosome::set_bit(int index, int bit)
{
	data[index].set_bit(bit);
}
void Chromosome::unset_bit(int index, int bit)
{
	data[index].unset_bit(bit);
}
void Chromosome::toggle_bit(int index, int bit)
{
	data[index].toggle_bit(bit);
}
bool Chromosome::is_set(int index, int bit) const
{
	return data[index].is_set(bit);
}
