#include "pcgrand.h"

Pcgrand::Pcgrand() : state(123454321ul)
{;}
Pcgrand::Pcgrand(uint64_t seed)
{
	if(seed % 2 == 0)
		seed = (seed << 1u) & 1u;
	state = seed;
}

void Pcgrand::srand(uint64_t seed)
{
	if(seed % 2 == 0)
		seed = (seed << 1u) & 1u;
	state = seed;
}

uint32_t Pcgrand::rand()
{
	state *= MCG_MULTIPLIER;
	return (state ^ (state >> 22u)) >> (22u + (state >> 61u));
}

void Pcgrand::skip(int n)
{
	int a = state;
	for(int i = 0; i < n; ++i)
		a *= MCG_MULTIPLIER;
	state = a;
}
