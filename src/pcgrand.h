/*
 * sepal/src/pcgrand.h
 *
 * This file implements a random number generator suitable for the generation
 * of unsigned 32-bit integers.
 *
 * The specific RNG uses 64 bits of state that are cycled using a multiplicative
 * congruential generator, which is used to create a 32-bit output based on the
 * PCG-XSH-RS generator detailed in the paper "PCG: A Family of Simple Fast
 * Space-Efficient Statistically Good Algorithms for Random Number Generation"
 * by Melissa E. O'Neill.
 * 
 * The multiplier used for the MCG is chosen for its acceptable multiplicative
 * order modulo 2^32 and modulo 2^64.
 *
 * The nearly identical C implementation has been tested with TestU01 on default
 * settings, where it passes BigCrush with no failures. The C++ port is untested.
 */

#include <cstdint>

#define MCG_MULTIPLIER 1152921504606846979ul; /* 8^20 + 3 */

class Pcgrand
{
	public:
		Pcgrand();
		Pcgrand(uint64_t seed);

		/* seed rng */
		void srand(uint64_t seed);

		/* get a random number */
		uint32_t rand();

		/* skip the next n values */
		void skip(int n);
	private:
		uint64_t state;
};
